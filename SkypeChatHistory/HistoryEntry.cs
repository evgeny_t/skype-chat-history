﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using SKYPE4COMLib;

namespace SkypeChatHistory
{
    public class HistoryEntry
    {
        private readonly string _entryFileName;

        public HistoryEntry(string entryFileName)
        {
            _entryFileName = entryFileName;

            if (!File.Exists(entryFileName))
                File.Create(entryFileName).Close();
        }

        public void Update(IChatMessage message)
        {
            if (message == null)
                throw new ArgumentNullException("message");

            using (var writer = new StreamWriter(
                _entryFileName, true, Encoding.UTF8))
            {
                var sb = new StringBuilder();
                sb.AppendFormat(
                    "[{0}]: ", 
                    message.Timestamp.ToString(CultureInfo.InvariantCulture))
                  .Append(message.Body);
                writer.WriteLine(sb.ToString());
            }
        }
    }
}