﻿
namespace SkypeChatHistory
{
    public interface IExceptionHandler
    {
        bool HandleComException(System.Runtime.InteropServices.COMException exception);
        bool HandleException(System.Exception exception);
    }
}